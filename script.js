let form = document.getElementById("root")

const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];

let ul = document.createElement("ul")
form.append(ul)

books.forEach(({author, name, price},i) => {
  try{
    if(books[i].author == undefined){
      throw new Error("Author not specified");
    } else if(books[i].name == undefined){
      throw new Error("Name not specified");
    } else if (books[i].price == undefined){
      throw new Error("Price not specified");
    } else{
      let li = document.createElement("li")
      ul.append(li)
      li.textContent = `Autor: ${author}, Name: ${name}, Price: ${price}`;
    }
  } catch (e){
    console.log(e)
  }
})



